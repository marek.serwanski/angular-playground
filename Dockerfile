#FROM node:12.7-alpine AS build
FROM trion/ng-cli-karma
WORKDIR /usr/src/app
COPY package.json ./
RUN npm install
COPY . .
EXPOSE 4200
RUN npm run build --prod
ENTRYPOINT ["npm","run", "start"]
