import {inject, TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {TranslateModule} from '@ngx-translate/core';
import {FormBuilder} from '@angular/forms';
import {AuthService} from '../../utils/security/auth.service';
import {TestDataFactory} from '../../utils/test/testdata.factory.spec';
import {RegisterComponent} from './regiter.component';

describe('Register Component Test', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RegisterComponent, FormBuilder, AuthService],
      imports: [HttpClientTestingModule, TranslateModule.forRoot(), RouterTestingModule]
    });
  });

  it('should register with correct data',
    inject([RegisterComponent, HttpTestingController], (component: RegisterComponent, httpClient: HttpTestingController) => {

      component.simpleForm = TestDataFactory.getRegisterData('testuser', 'pass123', 'pass123');

      component.register();

      const request = httpClient.expectOne('http://localhost:8080/register');
      expect(request.request.method).toEqual('POST');
    })
  );

  it('should not register with too wrong short password',
    inject([RegisterComponent, HttpTestingController], (component: RegisterComponent, httpClient: HttpTestingController) => {
      component.simpleForm = TestDataFactory.getRegisterData('testuser', 'pa', 'pa');
      component.register();
      httpClient.expectNone('http://localhost:8080/register');
    })
  );

  it('should not register with wrong confirm password',
    inject([RegisterComponent, HttpTestingController], (component: RegisterComponent, httpClient: HttpTestingController) => {
      component.simpleForm = TestDataFactory.getRegisterData('testuser', 'pass123', 'pass456');
      component.register();
      httpClient.expectNone('http://localhost:8080/register');
    })
  );

  it('should not register with epmty data',
    inject([RegisterComponent, HttpTestingController], (component: RegisterComponent, httpClient: HttpTestingController) => {
      component.simpleForm = TestDataFactory.getRegisterData('', '', '');
      component.register();
      httpClient.expectNone('http://localhost:8080/register');
    })
  );
});
