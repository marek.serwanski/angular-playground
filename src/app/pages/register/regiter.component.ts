import {Component} from '@angular/core';
import {RestClient} from '../../utils/rest/rest.client';
import {TranslateService} from '@ngx-translate/core';
import {FormBuilder, Validators} from '@angular/forms';
import {MustMatch} from '../../utils/forms/mustmatch.validator';
import {SimpleForm} from '../../utils/forms/simple.form';

@Component({
  selector: 'app-register-page',
  templateUrl: './register.component.html',
  providers: [RestClient]
})

export class RegisterComponent extends SimpleForm {

  successMsg: string;

  constructor(private restClient: RestClient, private translation: TranslateService, private formBuilder: FormBuilder) {
    super();
    this.initForm();
  }

  public register() {
    if (this.isValidForm()) {
      this.restClient.post('/register', this.simpleForm.getRawValue()).subscribe(() => {
          this.successMsg = this.translation.instant('register.accountCreatedInfo');
        },
        (err) => {
          this.error = this.translation.instant('register.registerError');
          this.submitted = false;
          console.log(err);
        });
    }
  }

  protected initForm() {
    this.simpleForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(3)]],
      confirmPassword: ['', Validators.required]
    }, {
      validator: MustMatch('password', 'confirmPassword')
    });
  }
}
