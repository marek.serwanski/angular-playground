import {inject, TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {TranslateModule} from '@ngx-translate/core';
import {FormBuilder} from '@angular/forms';
import {AuthService} from '../../utils/security/auth.service';
import {TestDataFactory} from '../../utils/test/testdata.factory.spec';
import {TaskComponent} from './task.component';

describe('Task Component Test', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TaskComponent, FormBuilder, AuthService],
      imports: [HttpClientTestingModule, TranslateModule.forRoot(), RouterTestingModule]
    });
  });

  it('should add task',
    inject([TaskComponent, HttpTestingController], (component: TaskComponent, httpClient: HttpTestingController) => {
      component.simpleForm = TestDataFactory.getTaskData('testtitle', 'testdesc');
      component.addTask();

      const addTaskRequest = httpClient.expectOne('http://localhost:8080/task');
      expect(addTaskRequest.request.method).toEqual('POST');
    })
  );

  it('should not add task with too short data',
    inject([TaskComponent, HttpTestingController], (component: TaskComponent, httpClient: HttpTestingController) => {
      component.simpleForm = TestDataFactory.getTaskData('te', 'te');
      component.addTask();
      httpClient.expectNone('http://localhost:8080/task');
    })
  );

  it('should not add task with tempty data',
    inject([TaskComponent, HttpTestingController], (component: TaskComponent, httpClient: HttpTestingController) => {
      component.simpleForm = TestDataFactory.getTaskData('', '');
      component.addTask();
      httpClient.expectNone('http://localhost:8080/task');
    })
  );

  it('should delete task',
    inject([TaskComponent, HttpTestingController], (component: TaskComponent, httpClient: HttpTestingController) => {
      component.deleteTask(3838);

      const addTaskRequest = httpClient.expectOne('http://localhost:8080/task/3838');
      expect(addTaskRequest.request.method).toEqual('DELETE');
    })
  );


});
