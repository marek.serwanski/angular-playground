export class Task {
  hashId: string;
  title: string;
  description: string;
}
