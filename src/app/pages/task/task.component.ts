import {Component} from '@angular/core';
import {RestClient} from '../../utils/rest/rest.client';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {AuthService} from '../../utils/security/auth.service';
import {Task} from './task.model';
import {TranslateService} from '@ngx-translate/core';
import {SimpleForm} from '../../utils/forms/simple.form';
import {FormBuilder, Validators} from '@angular/forms';

@Component({
  selector: 'app-task-page',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})

export class TaskComponent extends SimpleForm {

  tasks: Task[];

  constructor(private restClient: RestClient, private auth: AuthService, private modalService: NgbModal, private translate: TranslateService, private formBuilder: FormBuilder) {
    super();
    this.initForm();
    this.getTasks();
  }

  protected initForm() {
    this.simpleForm = this.formBuilder.group({
      title: ['', [Validators.required, Validators.minLength(3)]],
      description: ['', [Validators.required, Validators.minLength(3)]]
    });
  }

  private getTasks() {
    if (!this.auth.isAuthenticated()) {
      this.auth.gotoLoginPage();
    } else {
      this.tasksRequest();
    }
  }

  private tasksRequest() {
    this.restClient.http().get<Task[]>(this.restClient.getUrl('/task')).subscribe(
      response => {
        this.tasks = response;
      },
      (error) => {
          this.error = this.translate.instant('app.serverError');
          console.log(error);
      }
    );
  }

  addTask() {
    if (this.isValidForm()) {
      this.restClient.post('/task', this.simpleForm.getRawValue()).subscribe(
        () => {
          this.modalService.dismissAll();
          this.getTasks();
        },
        (error) => {
          this.logError(error);
        }
      );
    }
  }

  deleteTask(hashId: number) {
    this.restClient.delete('/task/' + hashId).subscribe(
      () => {
        this.getTasks();
      },
      (error) => {
        this.logError(error);
      }
    );
  }

  openTaskModal(content) {
    this.modalService.open(content, { windowClass: 'dark-modal' });
  }

  private logError(error) {
    this.error = this.translate.instant('app.serverError');
    console.log(error);
  }
}
