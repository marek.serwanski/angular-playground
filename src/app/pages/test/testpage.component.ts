import {Component, OnInit, OnDestroy} from '@angular/core';
import {RestClient} from '../../utils/rest/rest.client';

@Component({
  selector: 'app-test-page',
  templateUrl: './testpage.component.html',
  providers: [RestClient]
})

export class TestpageComponent implements OnInit, OnDestroy {

  public response: string;
  public error: string;

  constructor(private restClient: RestClient) {}

  public callApi(uri: string) {
    this.restClient.get(uri).subscribe(
      (data) => {
        this.response = JSON.stringify(data);
        this.error = '';
      },
      (err) => {
        console.log(err);
        this.response = '';
        this.error = 'Cannot get response :(';
      }
    );
  }

  // Check if router destroys component after going to other 'page'

  ngOnInit() {
    console.log('test component init');
  }

  ngOnDestroy() {
    console.log('test component destroy');
  }
}
