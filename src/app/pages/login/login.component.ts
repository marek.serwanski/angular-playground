import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../utils/security/auth.service';
import {TranslateService} from '@ngx-translate/core';
import {FormBuilder, Validators} from '@angular/forms';
import {SimpleForm} from '../../utils/forms/simple.form';

@Component({
  selector: 'app-login-page',
  templateUrl: './login.component.html'
})

export class LoginComponent extends SimpleForm{

  constructor(private auth: AuthService, private router: Router, private translation: TranslateService, private formBuilder: FormBuilder) {
    super();
    this.initForm();
  }

  public login() {
    if (this.isValidForm()) {
      this.auth.authenticate(this.simpleForm.getRawValue(), (responseStatus: number) => {
        if (responseStatus === 200) {
          this.router.navigateByUrl('/dashboard');
        } else if (responseStatus === 401){
          this.error = this.translation.instant('login.badCredentials');
        } else {
          this.error = this.translation.instant('app.serverError');
        }
      });
    }
  }

  protected initForm() {
    this.simpleForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(3)]]
    });
  }
}
