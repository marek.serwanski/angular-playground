import {inject, TestBed} from '@angular/core/testing';
import {LoginComponent} from './login.component';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TranslateModule} from '@ngx-translate/core';
import {FormBuilder} from '@angular/forms';
import {AuthService} from '../../utils/security/auth.service';
import {TestDataFactory} from '../../utils/test/testdata.factory.spec';

describe('Login Component Test', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoginComponent, FormBuilder, AuthService],
      imports: [HttpClientTestingModule, TranslateModule.forRoot(), RouterTestingModule]
    });
  });

  it('should login with correct data',
    inject([LoginComponent, AuthService], (component: LoginComponent, authService: AuthService) => {

      component.simpleForm = TestDataFactory.getDefaultUserdataForm();
      const authSpy = spyOn(authService, 'authenticate');

      component.login();

      expect(authSpy).toHaveBeenCalled();   // toHaveBeenCalledWith not works (the same values, function data.. magic)
    })
  );

  it('should not login with too short password',
    inject([LoginComponent, AuthService], (component: LoginComponent, authService: AuthService) => {

      component.simpleForm = TestDataFactory.getUserdataForm('test', 'qq');
      const authSpy = spyOn(authService, 'authenticate');

      component.login();

      expect(authSpy).toHaveBeenCalledTimes(0);
    })
  );

  it('should not login with empty data',
    inject([LoginComponent, AuthService], (component: LoginComponent, authService: AuthService) => {

      component.simpleForm = TestDataFactory.getUserdataForm('', '');
      const authSpy = spyOn(authService, 'authenticate');

      component.login();

      expect(authSpy).toHaveBeenCalledTimes(0);
    })
  );
});
