import {FormBuilder, Validators} from '@angular/forms';
import {MustMatch} from '../forms/mustmatch.validator';

export class TestDataFactory {

  public static username = 'testuser';
  public static password = 'pass123';
  private static formBuilder: FormBuilder = new FormBuilder();

  public static getUserdataForm(username: string, password: string) {
    return this.formBuilder.group({
      username: [username, Validators.required],
      password: [password, [Validators.required, Validators.minLength(3)]]
    });
  }

  public static getDefaultUserdataForm() {
    return this.getUserdataForm(this.username, this.password);
  }

  public static getRegisterData(username: string, password: string, confirmPassword: string) {
    return this.formBuilder.group({
      username: [username, Validators.required],
      password: [password, [Validators.required, Validators.minLength(3)]],
      confirmPassword: [confirmPassword, Validators.required]
    }, {
      validator: MustMatch('password', 'confirmPassword')
    });
  }

  public static getTaskData(title: string, description: string) {
    return this.formBuilder.group({
      title: [title , [Validators.minLength(3)]],
      description: [description, [Validators.required, Validators.minLength(3)]]
    });
  }
}
