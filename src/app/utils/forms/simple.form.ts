import {FormGroup} from "@angular/forms";

export abstract class SimpleForm {

  simpleForm: FormGroup;
  submitted: boolean = false;
  error: string;

  get form() {
    return this.simpleForm.controls;
  }

  //pitty that cannot put init into this constructor - Java is better
  protected abstract initForm();

  protected isValidForm() {
    this.submitted = true;
    return this.simpleForm.valid;
  }
}
