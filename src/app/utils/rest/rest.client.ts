import {Injectable} from "@angular/core";
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";

//client has defined host and other necessary settings. Has predefined simple get/post calls.

@Injectable({
  providedIn: 'root'
})
export class RestClient{

  // private DEFAULT_TIMEOUT: number = 3000;

  constructor (private httpClient: HttpClient) {}

  get(uri) {
    return this.httpClient.get(this.getUrl(uri))
  }

  post(uri, body) {
    return this.httpClient.post(this.getUrl(uri), body)
  }

  delete(uri) {
    return this.httpClient.delete(this.getUrl(uri));
  }

  postForResponse(uri, body) {
    return this.httpClient.post<any>(this.getUrl(uri), body, {observe: 'response'})
  }

  getWithHeaders(uri, {headers: headers}) {
    return this.httpClient.get(this.getUrl(uri), headers)
  }

  //for more complicated calls
  http() {
    return this.httpClient;
  }

  //todo: static ?
  getUrl(uri: string) {
    return environment.host + uri;
  }

}
