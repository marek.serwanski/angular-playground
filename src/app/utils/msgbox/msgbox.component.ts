import {Component, Input, OnChanges, OnInit} from "@angular/core";
import {animate, state, style, transition, trigger} from "@angular/animations";

@Component({
  selector: 'msg-box',
  templateUrl: './msgbox.component.html',
  styleUrls: ['./msgbox.component.css'],
  animations: [
    trigger('state', [
      state('active' , style({ opacity: 1 })),
      state('dissapearing', style({ opacity: 0 })),
      transition('void => active', [
        style({opacity: 0, height: '*', transform: 'scale(0)'}),
        animate(200)
      ]),
      transition('inactive => active', animate('200ms ease-in')),
      transition('active => dissapearing', animate('1500ms ease-out'))
    ])
  ]
})
export class MsgboxComponent implements OnInit, OnChanges {

  private DEFAULT_HIDE_TIMEOUT: number = 3000;

  @Input() public msg: string;
  @Input() public title: string;
  @Input() public hideErrorDetails: boolean;
  @Input() public dissapearing: boolean = false;
  @Input() public type : string = 'info';   //daclaring enum is not worth -> has to declare enum in every child. sick..

  public state : any = 'inactive';

  constructor() {}

  ngOnInit() {
    if (this.msg) {
      this.showMessage();
    }
  }

  ngOnChanges(changes) {
    if (this.msg) {
      this.showMessage();
    } else {
      this.hideMessage();
    }
  }

  private showMessage() {
    this.state = 'active';
    if (this.dissapearing) {
      this.hideAfterTimeout();
    }
  }

  private hideMessage() {
    this.state = 'inactive';
  }

  private hideAfterTimeout () {
    setTimeout(function () {
      this.state = 'dissapearing';
    }.bind(this), this.DEFAULT_HIDE_TIMEOUT);
  }

  public animationDone() {
    if (this.state === 'dissapearing') {
      this.hideMessage();
    }
  }

  public shouldShow(type : string) {
    return type === this.type && (this.state === 'active' || this.state === 'dissapearing');
  }
}
