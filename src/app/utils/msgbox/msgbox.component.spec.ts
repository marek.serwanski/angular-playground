import {TestBed, inject} from "@angular/core/testing";
import {MsgboxComponent} from "./msgbox.component";

describe('Msg Box Test', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MsgboxComponent]
    });
  });

  it('should be inactive on init',
    inject([MsgboxComponent], (component: MsgboxComponent) => {
      component.ngOnInit();

      expect(component.state).toEqual('inactive');
    })
  );

  it('should be active when there is msg',
    inject([MsgboxComponent], (component: MsgboxComponent) => {
      component.msg = 'test';

      component.ngOnInit();

      expect(component.state).toEqual('active');
    })
  );

  it('should be activated when msg comes',
    inject([MsgboxComponent], (component: MsgboxComponent) => {
      component.msg = 'test';

      component.ngOnChanges(null);

      expect(component.state).toEqual('active');
    })
  );

  it('should be deactivated when msg erases',
    inject([MsgboxComponent], (component: MsgboxComponent) => {
      component.msg = '';

      component.ngOnChanges(null);

      expect(component.state).toEqual('inactive');
    })
  );

});
