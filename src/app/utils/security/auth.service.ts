import {RestClient} from '../rest/rest.client';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';

@Injectable({providedIn: 'root'})
export class AuthService {

  private authTokenKey = 'x-auth-token';

  constructor(private restClient: RestClient, private router: Router) {}

  public authenticate(credentials, callback) {
    this.restClient.postForResponse('/login', credentials).subscribe(
      response => {
        this.storeAuthToken(response.headers.get(this.authTokenKey));
        return callback && callback(200);
      },
      (error) => {
        return callback && callback(error.status);
      }
    );
  }

  public logout() {
    this.restClient.post('/logoff', {}).subscribe(
  () => {
        localStorage.removeItem(this.authTokenKey);
      },
  (error) => {
        console.log(error);
      }
    );
  }

  public isAuthenticated() {
    return localStorage.getItem(this.authTokenKey) != null;
  }

  public getAuthToken() {
    return localStorage.getItem(this.authTokenKey);
  }

  // because key name should be in one one place. here
  public getAuthTokenKeyName() {
    return this.authTokenKey;
  }

  public gotoLoginPage() {
    this.router.navigate(['/login']);
  }

  private storeAuthToken(token: string) {
    localStorage.setItem(this.authTokenKey, token);
  }
}
