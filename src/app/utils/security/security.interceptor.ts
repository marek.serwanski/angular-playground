import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {XsrfService} from './xsrf.service';
import {AuthService} from './auth.service';
import {map} from 'rxjs/internal/operators';

// Interceptor do 2 things:
// 1st. set proper headers with necessary tokens to every request
// 2nd. check every response if it contains xsrf token (storing token and send it back is obvious csrf protection)
@Injectable()
export class SecurityInterceptor implements HttpInterceptor {

  constructor(public auth: AuthService, public xsrf: XsrfService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {

    let authToken = 'none';
    if (this.auth.isAuthenticated()) {
      authToken = this.auth.getAuthToken();
    }

    let xsrfToken = 'none';
    if (this.xsrf.hasStoredToken()) {
      xsrfToken = this.xsrf.getXsrfToken();
    }

    const clonedReq = req.clone({
      headers: req.headers
        .set('Access-Control-Allow-Origin', 'http://localhost:4200')
        .set('Access-Control-Allow-Methods', 'GET, POST, PATCH, PUT, DELETE, OPTIONS')
        .set('Access-Control-Allow-Credentials', 'true')
        .set('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers, Access-Control-Expose-Headers, X-AUTH-TOKEN, X-CSRF-TOKEN')
        .set(this.auth.getAuthTokenKeyName(), authToken)
        .set(this.xsrf.getXsrfTokenKey(), xsrfToken)
    });

    return next.handle(clonedReq).pipe(
      map((event: HttpEvent<any>) => {
        const tokenKey = this.xsrf.getXsrfTokenKey();
        if (event instanceof HttpResponse && event.headers.has(tokenKey)) {
          this.xsrf.storeXsrfToken(event.headers.get(tokenKey));
        }
        return event;
      }));
  }
}
