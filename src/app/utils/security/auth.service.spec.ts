import {inject, TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {TranslateModule} from '@ngx-translate/core';
import {AuthService} from './auth.service';
import {TestDataFactory} from '../test/testdata.factory.spec';

describe('Auth Service Test', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthService],
      imports: [HttpClientTestingModule, TranslateModule.forRoot(), RouterTestingModule]
    });
  });

  it('should call login',
    inject([AuthService, HttpTestingController], (service: AuthService, httpClient: HttpTestingController) => {

      service.authenticate(TestDataFactory.getDefaultUserdataForm().getRawValue(), 'callback');

      const request = httpClient.expectOne('http://localhost:8080/login');
      expect(request.request.method).toEqual('POST');
    })
  );

  it('should call logout',
    inject([AuthService, HttpTestingController], (service: AuthService, httpClient: HttpTestingController) => {

      service.logout();

      const request = httpClient.expectOne('http://localhost:8080/logoff');
      expect(request.request.method).toEqual('POST');
    })
  );

});
