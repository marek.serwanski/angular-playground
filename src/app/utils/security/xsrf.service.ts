import {Injectable} from '@angular/core';

@Injectable({providedIn: 'root'})
export class XsrfService {

  private xsrfTokenKey = 'x-csrf-token';

  constructor() {}

  public hasStoredToken() {
    return localStorage.getItem(this.xsrfTokenKey) != null;
  }

  public getXsrfToken() {
    return localStorage.getItem(this.xsrfTokenKey);
  }

  public storeXsrfToken(token: string) {
    localStorage.setItem(this.xsrfTokenKey, token);
  }

  // because key name should be in one one place. here
  public getXsrfTokenKey() {
    return this.xsrfTokenKey;
  }

}
