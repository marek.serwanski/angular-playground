import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './root/app.component';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AppRoutingModule} from './pages/app-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {TestpageComponent} from './pages/test/testpage.component';
import {MsgboxComponent} from './utils/msgbox/msgbox.component';
import {DashboardComponent} from './pages/dashboard/dashboard.component';
import {NgSelectModule} from '@ng-select/ng-select';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule, HttpClientXsrfModule} from '@angular/common/http';
import {LoginComponent} from './pages/login/login.component';
import {RegisterComponent} from './pages/register/regiter.component';
import {TaskComponent} from './pages/task/task.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AuthService} from './utils/security/auth.service';
import {SecurityInterceptor} from './utils/security/security.interceptor';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';


@NgModule({
  declarations: [
    AppComponent,
    TestpageComponent,
    DashboardComponent,
    LoginComponent,
    RegisterComponent,
    TaskComponent,
    MsgboxComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    NgSelectModule,
    BrowserAnimationsModule,
    HttpClientModule,
    HttpClientXsrfModule.withOptions({
      cookieName: 'XSRF-TOKEN',
      headerName: 'X-CSRF-TOKEN'
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    NgbModule
  ],
  providers: [AuthService, { provide: HTTP_INTERCEPTORS, useClass: SecurityInterceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
