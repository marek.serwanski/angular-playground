import {Component} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {Router} from '@angular/router';
import {AuthService} from '../utils/security/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [TranslateService, AuthService]
})
export class AppComponent {

  constructor(private translate: TranslateService, private auth: AuthService, private router: Router) {
    this.translate.setDefaultLang('pl');
    this.translate.use('pl');
  }

  public logout() {
    this.auth.logout();
    this.router.navigateByUrl('/login');
  }

  public authenticated() {
    return this.auth.isAuthenticated();
  }

  public switchLanguage() {
    if (this.translate.currentLang == 'en') {
      this.setLanguage('pl');
    } else {
      this.setLanguage('en');
    }
  }

  private setLanguage(language: string) {
    this.translate.setDefaultLang(language);
    this.translate.use(language);
  }
}
